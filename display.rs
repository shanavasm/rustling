use std::fmt;

struct Structure(i32);

impl fmt::Display for Structure {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

struct Point {
    x: i32,
    y: i32
}

impl fmt::Display for Point {
    fn fmt (&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "x: {}, y: {}", self.x, self.y)
    }
}

struct Complex {
    r: f64,
    i: f64
}

impl fmt::Display for Complex {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} + {}i", self.r, self.i)
    }
}

impl fmt::Debug for Complex {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Complex {{ real: {}, imag: {} }}", self.r, self.i)
    }
}

fn main() {
    let a = Structure(4);
    println!("a is {}", a);

    let b = Point{ x: 4, y: 5 };
    println!("{}", b);

    let c = Complex { r: 8.6, i: 9.0};

    println!("{}", c);
    println!("{:?}", c);
}
