#[derive(Debug)]
struct Structure(i32);

#[derive(Debug)]
struct Deep(Structure);

fn main() {
    println!("months {:?}", 3);

    println!("{:?}", Structure(3));
    println!("{:?}", Deep(Structure(8)));
}
