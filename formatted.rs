fn main() {
    println!("{} days", 30);
    // numbered arguments
    println!("{0}, this is {1}, {0}, and this is {1}", "Alice", "bob");

    // named arguments
    println!("subject is {subject}, verb is {verb} and object is {obj} ",
             obj = "Object", subject = "Sub", verb = "verb");

    // special formatting
    // binary
    println!("{} of {:b} people know binay", 1, 2);

    // padding
    println!("{number:>0width$}", number = 1, width = 6);

    let pi = 3.141592;
    println!("{:.2}", pi);

}
